//
//  JKFileOperationViewController.swift
//  JKSwiftFileOperation
//
//  Created by 王冲 on 2018/4/17.
//  Copyright © 2018年 希艾欧科技有限公司. All rights reserved.
//

import UIKit
import Foundation
// MARK: 屏幕的宽高
let JKscreenW: CGFloat = UIScreen.main.bounds.width
let JKscreenH: CGFloat = UIScreen.main.bounds.height

class JKFileOperationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 基本的设置
        navSet()
        print("根路径==\(NSHomeDirectory())")
        
        let string = "hhh"
        
        print("字符串的长度=\(string.count)")
        
        getSize()
    
    }
    
    // MARK: 15.计算文件夹或者文件的大小
    func getSize(){
        
        let path = JKFilePathOperationCategory.jKDocuments()
        
        let  size = JKFilePathOperationCategory.getSize(folderPath: path as String);
        print("结果=\(size)")
    }
    
    // MARK: 14.获得文件夹的属性
    func jKGetFileAttributes() {
        
        let docPath = NSHomeDirectory() + "/Documents/我的笔记.text"
        let attributes = JKFilePathOperationCategory.jKGetFileAttributes(filePath: docPath)
        
        print("创建时间：\(attributes[FileAttributeKey.creationDate]!)")
        print("修改时间：\(attributes[FileAttributeKey.modificationDate]!)")
        print("文件大小：\(attributes[FileAttributeKey.size]!)")

    }
    
    // MARK: 13.读取图片
    func jKreadImage() {

        let image = UIImageView(frame: CGRect(x: 50, y: 100, width: 50, height: 50))
        view.addSubview(image)
        
        let path = NSHomeDirectory() + "/Documents/2.png"
        let imageFile =  JKFilePathOperationCategory.jKReadImageFile(readPath: path)
        
        if imageFile.imageStatus
        {
            print("有图片")
            image.image = imageFile.imege as? UIImage
            
        }else{
            
            print("没有图片")
        }
    }
    
    // MARK: 12.读出文本文件内容
    func jKreadText() {
        
        let path = NSHomeDirectory() + "/Documents/我的笔记.text"
        let readHandler =  FileHandle(forReadingAtPath: path)
        let data = readHandler?.readDataToEndOfFile()
        let readString = String(data: data!, encoding: String.Encoding.utf8)
        print("文件内容: \(String(describing: readString))")
        
        let label = UILabel(frame: CGRect(x: 0, y: 100, width: JKscreenW, height: 50))
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = UIColor.purple
        label.backgroundColor = UIColor.white
        label.text = readString
        view.addSubview(label)
        
    }
    
    //MARK: 9.文件
    func jKMoveFile() {
        
        let fomePath = JKFilePathOperationCategory.jKDocuments() + "/JKPdf"
        let toPath = JKFilePathOperationCategory.jKTmp() + "/JKPdf"
        
        JKFilePathOperationCategory.jKMoveFile(fromeFile: fomePath as NSString, toFile: toPath as NSString)
    }
    
    //MARK: 8.复制文件
    func jKCopyFile() {
        
        let fomePath = JKFilePathOperationCategory.jKDocuments() + "/动画乐园.text"
        let toPath = JKFilePathOperationCategory.jKTmp() + "/copyed.text"
        
        JKFilePathOperationCategory.jKCopyFile(fromeFile: fomePath as NSString, toFile: toPath as NSString)
    }
    
    //MARK: 7.写入文件
    func jKWriteToFile(){
       
        //* 文字
        let createStatus = JKFilePathOperationCategory().jKCreateFile(fileName: "test.text", baseFilePath: JKFilePathOperationCategory.jKDocuments() as NSString)
        let info = "动画乐园欢迎你" as String
        JKFilePathOperationCategory.jKWriteToFile(writeType: 1, content: info, writePath: createStatus.filePath as String)
        //*/
        
        /* 图片
        let filePath = NSHomeDirectory() + "/Documents/图片/testimage.png"
        let image = UIImage(named: "testimage.png")
        let data:Data = UIImagePNGRepresentation(image!)!
        try? data.write(to: URL(fileURLWithPath: filePath))
        */
        
        /*
        let createStatus = JKFilePathOperationCategory().jKCreateFile(fileName: "array.plist", baseFilePath: JKFilePathOperationCategory.jKDocuments() as NSString)
  
        let filePath = NSHomeDirectory() + "/Documents/array.plist"
        let array = NSArray(objects: "我","❤️","你")
        array.write(toFile: filePath, atomically: true)
        */
        
        /**
        let createStatus = JKFilePathOperationCategory().jKCreateFile(fileName: "dictionary.plist", baseFilePath: JKFilePathOperationCategory.jKDocuments() as NSString)
        
        let filePath = NSHomeDirectory() + "/Documents/dictionary.plist"
        let dictionary = NSDictionary(dictionary: ["name":"JK","age":"26"])
        dictionary.write(toFile: filePath, atomically: true)
        */
        
        //JKFilePathOperationCategory.jKWriteToFile(writeType: 1, content: info, writePath: createStatus.filePath as String)
        
    }
    
    //MARK: 6.创建文件
    func jKCreateFile() {
        
       let createStatus = JKFilePathOperationCategory().jKCreateFile(fileName: "我的笔记.text", baseFilePath: JKFilePathOperationCategory.jKDocuments() as NSString)
        print("创建的文件路径=\(createStatus.filePath) 是否创建成功=\(createStatus.createStatus)")
    }
    
    // MARK: 5.深度搜索
    func jKDeepSearchAllFiles() {
        
        let customPath = JKFilePathOperationCategory.jKHomeDirectory()
        let array = JKFilePathOperationCategory().jKDeepSearchAllFiles(folderName: customPath as NSString)
        print("array=\(array)")
    }
    
    // MARK: 4.读取指定目录路径下的文件、子目录及符号链接的列表(只寻找一层)
    func jKShallowSearchAllFiles(){
        
        let customPath = JKFilePathOperationCategory.jKHomeDirectory()
        let array = JKFilePathOperationCategory().jKShallowSearchAllFiles(folderName: customPath as NSString)
        print("array=\(array)")
    }
    
    // MARK: 3.获取一个文件夹下面的所有文件以及文件夹路径的名字
    func jKGetAllFileNames(){
        
        let customPath = JKFilePathOperationCategory.jKHomeDirectory()
        let array = JKFilePathOperationCategory().jKGetAllFileNames(folderName: customPath as NSString)
        print("array=\(array)")
    }
    
    // MARK: 2.删除文件夹
    func removeFolder(path:NSString) {
        
        let customPath = JKFilePathOperationCategory.jKDocuments() + "/\(path)"
        JKFilePathOperationCategory().jKRemovefolder(folderName: customPath as NSString)
    }
    
    
    // MARK: 1.自定义路径创建文件夹
    func createFolder(path:NSString) {

        let customPath = JKFilePathOperationCategory.jKDocuments() + "/\(path)"
        let pathStr = JKFilePathOperationCategory().jKCreateFolder(folderName: customPath as NSString);
        print("pathStr==\(pathStr)")
    }
    
    // MARK: 基本设置
    func navSet(){
        
        title = "文件操作"
        navigationController?.navigationBar.barTintColor = UIColor.white
        view.backgroundColor = UIColor.green
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    

}
